<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test create user.
     *
     * @return void
     */
    public function testUserCreate()
    {
        \App\User::create([
            'name' => 'User',
            'email' => 'user@crossx.com',
            'password' => bcrypt('123456'),
        ]);

        $this->assertDatabaseHas('users',[ 'name' => 'user', 'email' => 'user@crossx.com']);

    }

}
