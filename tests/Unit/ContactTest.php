<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test create user.
     *
     * @return void
     */
    public function testContactCreate()
    {
        $user = \App\User::create([
            'name' => 'User',
            'email' => 'user@crossx.com',
            'password' => bcrypt('123456'),
        ]);

        $this->assertDatabaseHas('users',[ 'name' => 'user', 'email' => 'user@crossx.com']);


        \App\Contact::create([
            'name' => 'NameTest',
            'lastname' => 'LastnameTest',
            'email' => 'emailteste@emailteste.com',
            'user_id' => $user->id,
            'numbers' => ['123456789','987654321']]);

        $this->assertDatabaseHas('contacts',
            [ 'name' => 'NameTest',
            'lastname' => 'LastnameTest',
            'email' => 'emailteste@emailteste.com',
            'user_id' => $user->id
            ]);

    }

}

