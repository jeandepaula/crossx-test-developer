<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(\Auth::check())
    return redirect('/contacts');
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('user/{user}','UserController@show');
Route::post('user/{user}/avatar','UserController@update_avatar');

Route::resource('reports', 'ReportController');
Route::resource('contacts', 'ContactController');


// Route::get('/{vue_capture?}', function () {
//     return view('index');
// })->where('vue_capture', '[\/\w\.-]*');