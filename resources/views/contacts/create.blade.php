@extends('home')

@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Adicionar Contato</h1>
        <div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('contacts.store') }}">
                @csrf
                <div class="form-group">
                    <label for="first_name">Nome:</label>
                    <input type="text" class="form-control" name="name" />
                </div>

                <div class="form-group">
                    <label for="last_name">Sobrenome:</label>
                    <input type="text" class="form-control" name="lastname" />
                </div>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" name="email" />
                </div>

                <table class="table table-bordered table-hover" id="tab_logic">
                    <tbody>
                        <tr id='addr0'>
                            <td style="display: none;">1</td>
                            <td>
                                <input type="number" name='numbers[]' placeholder='Telefone' class="form-control" />
                            </td>
                        </tr>
                        <tr id='addr1'></tr>
                    </tbody>
                </table>

                <div class="form-group">
                    <a id="add_row" class="btn btn-danger pull-left">+</a> <a id="delete_row" class="btn btn-primary pull-left">-</a>
                </div>

                <button type="submit" class="btn btn-primary">Adicionar</button>
            </form>
        </div>
    </div>
</div>
@endsection