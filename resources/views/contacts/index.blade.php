@extends('home')

@section('main')
<!-- Modal Dialog Delete -->
<div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Atenção!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <p>Você tem certeza que desejar remover esse contato?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" id="confirm">Remover</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal create new contact -->
<div class="modal fade" id="createContact" role="dialog" aria-labelledby="createContactLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Adicionar Contato</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('contacts.store') }}">
          @csrf
          <div class="form-group">
            <label for="first_name">Nome:</label>
            <input type="text" class="form-control" name="name" />
          </div>

          <div class="form-group">
            <label for="last_name">Sobrenome:</label>
            <input type="text" class="form-control" name="lastname" />
          </div>

          <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" class="form-control" name="email" />
          </div>

          <table class="table table-bordered table-hover" id="tab_logic">
            <tbody>
              <tr id='addr0'>
                <td style="display: none;">1</td>
                <td>
                  <input type="number" name='numbers[]' placeholder='Telefone' class="form-control" />
                </td>
              </tr>
              <tr id='addr1'></tr>
            </tbody>
          </table>

          <div class="form-group">
            <a id="add_row" class="btn btn-danger pull-left">+</a> <a id="delete_row" class="btn btn-primary pull-left">-</a>
          </div>

          <button type="submit" class="btn btn-primary">Adicionar</button>
        </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" id="confirm">Remover</button>
      </div> -->
    </div>
  </div>
</div>


<div class="modal fade" id="dynamic-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body"></div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
    @endif
  </div>
  <div>
    <button style="margin: 19px;" class="btn btn-primary" type="button" data-toggle="modal" data-target="#createContact">Novo contato</button>
  </div>
  <div>
    <a style="margin: 19px;" href="{{ route('reports.index')}}" class="btn btn-primary">Relatórios</a>
  </div>
  <div class="col-sm-12">
    <h1 class="display-3">Contatos</h1>
    <form action="contacts" method="get">
      <!-- Grid row -->
      <div class="form-row">
        <!-- Grid column -->
        <div class="col-lg-11">
          <!-- Material input -->
          <div class="md-form form-group">
            @if(isset($_GET['name']))
            <input type="text" class="form-control" name="name" placeholder="Pesquisar" value="{{$_GET['name']}}">
            @else
            <input type="text" class="form-control" name="name" placeholder="Pesquisar" value="">
            @endif
          </div>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-1">
          <!-- Material input -->
          <div class="md-form form-group">
            <button type="submit" class="btn form-control btn-danger" type="button">
              🔎
            </button>
          </div>
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->


    </form>
    <table class="table table-striped">
      <thead>
        <tr>

          <td>Name</td>
          <td>Email</td>
          <td>Telefone(s)</td>
          <td colspan=2>Ações</td>
        </tr>
      </thead>
      <tbody>
        @foreach($contacts as $contact)
        <tr>

          <td>{{$contact->name}} {{$contact->lastname}}</td>
          <td>{{$contact->email}}</td>
          <td>{{ implode(' - ', $contact->numbers) }}</td>
          <td>
            <button data-path="{{ route('contacts.edit',$contact->id) }}" class="btn btn-primary load-ajax-modal" role="button" data-toggle="modal" data-target="#dynamic-modal">
              <span class="glyphicon glyphicon-eye-open"></span> Editar
            </button>
          </td>
          <td>
            <form action="{{ route('contacts.destroy', $contact->id)}}" method="post">
              @csrf
              @method('DELETE')
              <button class="btn btn-xs btn-danger" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Atenção!" data-message="Você tem certeza em remover esse contato?">
                <i class="glyphicon glyphicon-trash"></i> Delete
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>
    </div>

    @endsection