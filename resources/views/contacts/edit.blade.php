<div class="row">
    <div class="col-sm-12">
        <h2 class="display-9">Atualize um contato</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('contacts.update', $contact->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="first_name">Primeiro nome:</label>
                <input type="text" class="form-control" name="name" value="{{ $contact->name }}" />
            </div>

            <div class="form-group">
                <label for="last_name">Sobrenome:</label>
                <input type="text" class="form-control" name="last_name" value="{{ $contact->lastname }}" />
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value="{{ $contact->email }}" />
            </div>

            <table class="table table-bordered table-hover" id="tab_logic_edit">
                <tbody>

                    @foreach ($contact->numbers as $key => $number)
                    <tr id='addr_edit0'>
                        <td style="display:none;">{{$key}}</td>
                        <td>
                            <input type="number" name='numbers[]' placeholder='Telefone' value="{{$number}}" class="form-control" />
                        </td>
                    </tr>
                    @endforeach
                    <tr id='addr_edit1'></tr>
                </tbody>
            </table>

            <div class="form-group">
                <a id="add_row_edit" class="btn btn-danger pull-left">+</a>
            </div>


            <button type="submit" class="btn btn-primary">Atualizar</button>
            <br />
            <br />
            *Caso não queira adicionar mais telefones, deixe em branco.
        </form>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    var i=1;
    $("#add_row_edit").click(function(){b=i-1
        // alert('teste');
        var semvalue = '<td style="display: none;">'+i+'</td> <td><input type="number" name="numbers[]" placeholder="Telefone" value="" class="form-control"></td>';
        $('#addr_edit'+i).html(semvalue).find('td:first-child').html(i+1);
        $('#tab_logic_edit').append('<tr id="addr_edit'+(i+1)+'"></tr>');
        i++;
    });

});
</script>
