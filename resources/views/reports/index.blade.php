@extends('home')

@section('main')

<div class="row">
<h1 class="display-4">Relatório</h2>
  <div class="col-sm-12">
    <br/>
    <h2 class="display-8">Contatos com telefone</h2>
    <table class="table table-striped">
      <thead>
        <tr>

          <td>Name</td>
          <td>Email</td>
          <td>Telefone(s)</td>
        </tr>
      </thead>
      <tbody>
        @foreach($contacts_ct as $contact)
        <tr>

          <td>{{$contact->name}} {{$contact->lastname}}</td>
          <td>{{$contact->email}}</td>
          <td>{{ implode(' - ', $contact->numbers) }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>
</div>


<div class="row">

  <div class="col-sm-12">
  <h2 class="display-8">Contatos sem telefone</h2>
    <table class="table table-striped">
      <thead>
        <tr>

          <td>Name</td>
          <td>Email</td>
          <td>Telefone(s)</td>
        </tr>
      </thead>
      <tbody>
        @foreach($contacts_st as $contact_st)
        <tr>

          <td>{{$contact_st->name}} {{$contact_st->lastname}}</td>
          <td>--</td>
          <td>-</td>
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>
</div>



@endsection