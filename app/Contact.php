<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','lastname','email','user_id','numbers'];

    protected $casts = [
        'numbers' => 'array',
    ];
}
