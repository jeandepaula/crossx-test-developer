<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return Storage::get($user->avatar);
        // return $user->avatar;
        // return Storage::download($user->avatar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function update_avatar(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'file' => 'required|image'
        ]);

        if($user->avatar != 'public/avatars/default.jpg')
        Storage::delete($user->avatar);

        $path = Storage::put('public/avatars', $validatedData['file']);

        $user->avatar = $path;
        $user->save();

        return str_replace("public/", "", $path);
    }

}
