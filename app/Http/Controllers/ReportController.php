<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts_ct = Contact::where('user_id',Auth::user()->id)->where('numbers','like','%,%')->orWhere('numbers','like','[%"%]')->get();
        $contacts_st = Contact::where('user_id',Auth::user()->id)->where('numbers',NULL)->orWhere('numbers','like','[]')->get();

        return view('reports.index', compact('contacts_ct','contacts_st'));
    }


}
