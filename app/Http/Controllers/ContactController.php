<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $name = $request->input('name');

        $query = Contact::select();

        if ($name){
            $query = $query->where('name','like', '%'.$name.'%')->orWhere('lastname','like', '%'.$name.'%');
        }

        $contacts = $query->where('user_id',Auth::user()->id)->get();

        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|min:2|max:50',
            'lastname' => 'nullable|string|min:5|max:50',
            'email' => 'nullable|email',
            'numbers' => 'nullable|array',
        ]);

        $validatedData['user_id'] = Auth::user()->id;
        $validatedData['numbers'] = array_filter($validatedData['numbers']);

        Contact::create($validatedData);

        return redirect('/contacts')->with('success', 'Contato salvo!');
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        return $contact;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        $contact = $contact;
        return view('contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|min:5|max:50',
            'lastname' => 'nullable|string|min:5|max:50',
            'email' => 'nullable|email',
            'numbers' => 'nullable|array',
            'numbers.*' => 'nullable|min:8|max:20',
        ]);

        if(isset($validatedData['numbers']))
        $validatedData['numbers'] = array_filter($validatedData['numbers']);

        $contact->fill($validatedData);
        $contact->save();

        return redirect('/contacts')->with('success', 'Contato atualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();

        return redirect('/contacts')->with('success', 'Contato removido com sucesso!');
    }
}
